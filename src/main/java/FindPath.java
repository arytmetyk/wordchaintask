import java.io.File;
import java.io.IOException;
import java.util.*;


public class FindPath {
    Set<String> dictionary;

    public FindPath() {
        loadDictionary();
    }

    private void loadDictionary() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("wordlist.txt").getFile());

        List<String> loadedWordList = new LinkedList<String>();

        try {
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                line = line.toLowerCase();
                loadedWordList.add(line);
            }

            scanner.close();
        } catch (IOException e) {
            System.out.println("Error during loading dictionary list from resources!");
        }

        dictionary = new HashSet<String>(loadedWordList);
    }

    public int getDictionarySize() {
        return dictionary.size();
    }

    Set<String> getNeighbours(String word) {
        word = word.toLowerCase();
        HashSet<String> neighbours = new HashSet<String>();
        for (int i = 0; i < word.length(); i++) {// FOR EACH POSITION AT OUR STRING
            for (int j = 'a'; j <= 'z'; j++) {// FOR EACH POSSIBLE CHARACTER FROM ALPHABET
                if (j == word.charAt(i))
                    continue;
                String possibleNeighbour = word.substring(0, i) + (char) j + word.substring(i + 1);
                if (dictionary.contains(possibleNeighbour)) {// CHECK IF THIS WORD EXIST IN DICTIONARY
                    neighbours.add(possibleNeighbour);
                }

            }
        }

        return neighbours;
    }

    public List<String> findShortestPath(String from, String to) {
        from.toLowerCase();
        to.toLowerCase();
        Set<String> visited = new HashSet<String>();
        Map<String, String> previous = new HashMap<String, String>();
        Queue<String> queue = new LinkedList<String>();
        queue.add(from);
        previous.put(from, "#"); // TO AVOID NULL RESULT FOR FIRST WORD

        while (!queue.isEmpty()) {// BREADTH-FIRST SEARCH GRAPH SEARCH IMPLEMENTATION
            String currentWord = queue.poll();
            visited.add(currentWord);
            if (currentWord.equals(to)) {
                break;
            }
            for (String neighbour : getNeighbours(currentWord)) {
                if (!previous.containsKey(neighbour)) {
                    previous.put(neighbour, currentWord);
                    queue.add(neighbour);
                }
            }
        }

        if (!previous.containsKey(to)) {
            throw new IllegalArgumentException("There is no chain for given parameters!");
        }

        List<String> resultChain = new LinkedList<String>();
        String currentWord = to;
        while (!currentWord.equals(from)) {
            resultChain.add(currentWord);
            currentWord = previous.get(currentWord);
        }
        resultChain.add(from);
        Collections.reverse(resultChain);

        return resultChain;
    }


}
