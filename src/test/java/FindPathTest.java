import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashSet;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


public class FindPathTest {
    static FindPath findPathInstance;

    @BeforeClass
    public static void setUp() {
        findPathInstance = new FindPath();
    }

    @Test
    public void shouldLoadWordList() throws Exception {
        assertTrue(findPathInstance.getDictionarySize() > 0);
    }

    @Test
    public void shouldGetCorrectNeighboursForWord() throws Exception {
        Set<String> expectedNeighbours = new HashSet<String>();
        expectedNeighbours.add("airplane");

        Set<String> resultSet = findPathInstance.getNeighbours("airplaXe");

        assertEquals(expectedNeighbours, resultSet);


        expectedNeighbours.clear();
        expectedNeighbours.add("tamera");
        expectedNeighbours.add("cambra");
        expectedNeighbours.add("calera");

        resultSet = findPathInstance.getNeighbours("camera");

        assertEquals(expectedNeighbours, resultSet);
    }

    @Test
    public void shouldNotTreatItselfLikeANeighbour() throws Exception {
        String testedWord = "airplane";
        Set<String> resultSet = findPathInstance.getNeighbours(testedWord);

        assertFalse(resultSet.contains(testedWord));
    }

    @Test
    public void shouldFindTheShortestChain() throws Exception {

        List<String> resultChain = findPathInstance.findShortestPath("lead", "gold");
        System.out.println(resultChain);
        assertTrue(resultChain.size() <= 4);

        resultChain = findPathInstance.findShortestPath("cat", "dog");
        System.out.println(resultChain);
        assertTrue(resultChain.size() <= 4);

        resultChain = findPathInstance.findShortestPath("wallet", "bullet");
        System.out.println(resultChain);
        assertTrue(resultChain.size() <= 3);

        resultChain = findPathInstance.findShortestPath("mouse", "holes");
        System.out.println(resultChain);
        assertTrue(resultChain.size() <= 7);
    }

    @Test
    public void shouldNotFindNotExistingChain() throws Exception {
        try {
            findPathInstance.findShortestPath("wallet", "gossip");
            throw new RuntimeException("IllegalArgumentException before expected");
        } catch (IllegalArgumentException e) {
            System.out.println("Expected exception catched, going on.");
        }

        try {
            findPathInstance.findShortestPath("dog", "mouse");
            throw new RuntimeException("IllegalArgumentException before expected");
        } catch (IllegalArgumentException e) {
            System.out.println("Expected exception catched, going on.");
        }
    }
}