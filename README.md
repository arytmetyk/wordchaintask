This is my solution for task from `http://codekata.com/kata/kata19-word-chains/`.  
Dictionary is taken from `codekata.com/data/wordlist.txt`.  
Solution is made for words containing only english letters from a to z.  
  
    
Complexity: O ( (number of words in dictionary) * (alphabet size) * (word length) ) 
  
The way of finding the shortest chain is graph breadth-first search.  
The way of finding graph neighbours for each word is changing any word letter to any allowed letter from the alphabet.  
Using a hashset allows to check if given word exist in dictionary at O(1) time.

  
    
Bartlomiej Drozd